﻿// ********************************************************** //
// Description: This is a standalone debounce function extracted from
// lodash core. It is useful when the page only needs 
// debounce function not complete lodash. 
// Don't add this script if the page already use lodash.
// ********************************************************** //

;(function() {
    /* Built-in method references for those with the same name as other `lodash` methods. */
    var nativeMax = Math.max;
    var nativeMin = Math.min;
    var reIsBinary = '/^0b[01]+$/i';
    var now = Date.now;
    

    /** Used to determine if values are of the language type `Object`. */
    var objectTypes = {
        'function': true,
        'object': true
    };

    /** Detect free variable `exports`. */
    var freeExports = (objectTypes[typeof exports] && exports && !exports.nodeType)
      ? exports
      : undefined;

    /** Detect free variable `module`. */
    var freeModule = (objectTypes[typeof module] && module && !module.nodeType)
      ? module
      : undefined;

    /** Detect the popular CommonJS extension `module.exports`. */
    var moduleExports = (freeModule && freeModule.exports === freeExports)
      ? freeExports
      : undefined;

    /** Detect free variable `global` from Node.js. */
    var freeGlobal = checkGlobal(freeExports && freeModule && typeof global == 'object' && global);

    /** Detect free variable `self`. */
    var freeSelf = checkGlobal(objectTypes[typeof self] && self);

    /** Detect free variable `window`. */
    var freeWindow = checkGlobal(objectTypes[typeof window] && window);

    /** Detect `this` as the global object. */
    var thisGlobal = checkGlobal(objectTypes[typeof this] && this);

    var root = freeGlobal ||
    ((freeWindow !== (thisGlobal && thisGlobal.window)) && freeWindow) ||
      freeSelf || thisGlobal || Function('return this')();

    /**
    
     * Creates a debounced function that delays invoking `func` until after `wait`
     * milliseconds have elapsed since the last time the debounced function was
     * invoked. The debounced function comes with a `cancel` method to cancel
     * delayed `func` invocations and a `flush` method to immediately invoke them.
     * Provide `options` to indicate whether `func` should be invoked on the
     * leading and/or trailing edge of the `wait` timeout. The `func` is invoked
     * with the last arguments provided to the debounced function. Subsequent
     * calls to the debounced function return the result of the last `func`
     * invocation.
     *
     * **Note:** If `leading` and `trailing` options are `true`, `func` is
     * invoked on the trailing edge of the timeout only if the debounced function
     * is invoked more than once during the `wait` timeout.
     *
     * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
     * until to the next tick, similar to `setTimeout` with a timeout of `0`.
     *
     * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
     * for details over the differences between `debounce` and `throttle`.
     *
     * @since 0.1.0
     * @category Function
     * @param {Function} func The function to debounce.
     * @param {number} [wait=0] The number of milliseconds to delay.
     * @param {Object} [options={}] The options object.
     * @param {boolean} [options.leading=false]
     *  Specify invoking on the leading edge of the timeout.
     * @param {number} [options.maxWait]
     *  The maximum time `func` is allowed to be delayed before it's invoked.
     * @param {boolean} [options.trailing=true]
     *  Specify invoking on the trailing edge of the timeout.
     * @returns {Function} Returns the new debounced function.
     * @example
     *
     * // Avoid costly calculations while the window size is in flux.
     * jQuery(window).on('resize', debounce(calculateLayout, 150));
     *
     * // Invoke `sendMail` when clicked, debouncing subsequent calls.
     * jQuery(element).on('click', debounce(sendMail, 300, {
     *   'leading': true,
     *   'trailing': false
     * }));
     *
     * // Ensure `batchLog` is invoked once after 1 second of debounced calls.
     * const debounced = debounce(batchLog, 250, { 'maxWait': 1000 });
     * const source = new EventSource('/stream');
     * jQuery(source).on('message', debounced);
     *
     * // Cancel the trailing debounced invocation.
     * jQuery(window).on('popstate', debounced.cancel);
     */
    function debounce(func, wait, options) {
        var lastArgs,
            lastThis,
            result,
            timerId,
            lastCallTime = 0,
            lastInvokeTime = 0,
            leading = false,
            maxWait = false,
            trailing = true;

        if (typeof func != 'function') {
            throw new TypeError('Expected a function');
        }
        wait = toNumber(wait) || 0;
        if (isObject(options)) {
            leading = !!options.leading;
            maxWait = 'maxWait' in options && nativeMax(toNumber(options.maxWait) || 0, wait);
            trailing = 'trailing' in options ? !!options.trailing : trailing;
        }

        function invokeFunc(time) {
            var args = lastArgs,
                thisArg = lastThis;

            lastArgs = lastThis = undefined;
            lastInvokeTime = time;
            result = func.apply(thisArg, args);
            return result;
        }

        function leadingEdge(time) {
            // Reset any `maxWait` timer.
            lastInvokeTime = time;
            // Start the timer for the trailing edge.
            timerId = setTimeout(timerExpired, wait);
            // Invoke the leading edge.
            return leading ? invokeFunc(time) : result;
        }

        function remainingWait(time) {
            var timeSinceLastCall = time - lastCallTime,
                timeSinceLastInvoke = time - lastInvokeTime,
                result = wait - timeSinceLastCall;

            return maxWait === false ? result : nativeMin(result, maxWait - timeSinceLastInvoke);
        }

        function shouldInvoke(time) {
            var timeSinceLastCall = time - lastCallTime,
                timeSinceLastInvoke = time - lastInvokeTime;

            // Either this is the first call, activity has stopped and we're at the
            // trailing edge, the system time has gone backwards and we're treating
            // it as the trailing edge, or we've hit the `maxWait` limit.
            return (!lastCallTime || (timeSinceLastCall >= wait) ||
              (timeSinceLastCall < 0) || (maxWait !== false && timeSinceLastInvoke >= maxWait));
        }

        function timerExpired() {
            var time = now();
            if (shouldInvoke(time)) {
                return trailingEdge(time);
            }
            // Restart the timer.
            timerId = setTimeout(timerExpired, remainingWait(time));
        }

        function trailingEdge(time) {
            clearTimeout(timerId);
            timerId = undefined;

            // Only invoke if we have `lastArgs` which means `func` has been
            // debounced at least once.
            if (trailing && lastArgs) {
                return invokeFunc(time);
            }
            lastArgs = lastThis = undefined;
            return result;
        }

        function cancel() {
            if (timerId !== undefined) {
                clearTimeout(timerId);
            }
            lastCallTime = lastInvokeTime = 0;
            lastArgs = lastThis = timerId = undefined;
        }

        function flush() {
            return timerId === undefined ? result : trailingEdge(now());
        }

        function debounced() {
            var time = now(),
                isInvoking = shouldInvoke(time);

            lastArgs = arguments;
            lastThis = this;
            lastCallTime = time;

            if (isInvoking) {
                if (timerId === undefined) {
                    return leadingEdge(lastCallTime);
                }
                // Handle invocations in a tight loop.
                clearTimeout(timerId);
                timerId = setTimeout(timerExpired, wait);
                return invokeFunc(lastCallTime);
            }
            return result;
        }
        debounced.cancel = cancel;
        debounced.flush = flush;
        return debounced;
    }

    /**
     * Creates a throttled function that only invokes `func` at most once per
     * every `wait` milliseconds. The throttled function comes with a `cancel`
     * method to cancel delayed `func` invocations and a `flush` method to
     * immediately invoke them. Provide an options object to indicate whether
     * `func` should be invoked on the leading and/or trailing edge of the `wait`
     * timeout. The `func` is invoked with the last arguments provided to the
     * throttled function. Subsequent calls to the throttled function return the
     * result of the last `func` invocation.
     *
     * **Note:** If `leading` and `trailing` options are `true`, `func` is
     * invoked on the trailing edge of the timeout only if the throttled function
     * is invoked more than once during the `wait` timeout.
     *
     * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
     * for details over the differences between `_.throttle` and `_.debounce`.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Function
     * @param {Function} func The function to throttle.
     * @param {number} [wait=0] The number of milliseconds to throttle invocations to.
     * @param {Object} [options={}] The options object.
     * @param {boolean} [options.leading=true]
     *  Specify invoking on the leading edge of the timeout.
     * @param {boolean} [options.trailing=true]
     *  Specify invoking on the trailing edge of the timeout.
     * @returns {Function} Returns the new throttled function.
     * @example
     *
     * // Avoid excessively updating the position while scrolling.
     * jQuery(window).on('scroll', _.throttle(updatePosition, 100));
     *
     * // Invoke `renewToken` when the click event is fired, but not more than once every 5 minutes.
     * var throttled = _.throttle(renewToken, 300000, { 'trailing': false });
     * jQuery(element).on('click', throttled);
     *
     * // Cancel the trailing throttled invocation.
     * jQuery(window).on('popstate', throttled.cancel);
     */
    function throttle(func, wait, options) {
        var leading = true,
            trailing = true;

        if (typeof func != 'function') {
            throw new TypeError('Expected a function');
        }
        if (isObject(options)) {
            leading = 'leading' in options ? !!options.leading : leading;
            trailing = 'trailing' in options ? !!options.trailing : trailing;
        }
        return debounce(func, wait, {
            'leading': leading,
            'maxWait': wait,
            'trailing': trailing
        });
    }

    /**
     * Checks if `value` is classified as a `Function` object.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
     * @example
     *
     * _.isFunction(_);
     * // => true
     *
     * _.isFunction(/abc/);
     * // => false
     */
    function isFunction(value) {
        // The use of `Object#toString` avoids issues with the `typeof` operator
        // in Safari 8 which returns 'object' for typed array constructors, and
        // PhantomJS 1.9 which returns 'function' for `NodeList` instances.
        var tag = isObject(value) ? objectToString.call(value) : '';
        return tag == funcTag || tag == genTag;
    }

    /**
     * Checks if `value` is the [language type](https://es5.github.io/#x8) of `Object`.
     * (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is an object, else `false`.
     * @example
     *
     * _.isObject({});
     * // => true
     *
     * _.isObject([1, 2, 3]);
     * // => true
     *
     * _.isObject(_.noop);
     * // => true
     *
     * _.isObject(null);
     * // => false
     */
    function isObject(value) {
        var type = typeof value;
        return !!value && (type == 'object' || type == 'function');
    }

    /**
     * Converts `value` to a number.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to process.
     * @returns {number} Returns the number.
     * @example
     *
     * _.toNumber(3);
     * // => 3
     *
     * _.toNumber(Number.MIN_VALUE);
     * // => 5e-324
     *
     * _.toNumber(Infinity);
     * // => Infinity
     *
     * _.toNumber('3');
     * // => 3
     */
    function toNumber(value) {
        if (typeof value == 'number') {
            return value;
        }
        if (isSymbol(value)) {
            return NAN;
        }
        if (isObject(value)) {
            var other = isFunction(value.valueOf) ? value.valueOf() : value;
            value = isObject(other) ? (other + '') : other;
        }
        if (typeof value != 'string') {
            return value === 0 ? value : +value;
        }
        value = value.replace(reTrim, '');
        var isBinary = reIsBinary.test(value);
        return (isBinary || reIsOctal.test(value))
          ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
          : (reIsBadHex.test(value) ? NAN : +value);
    }

    function checkGlobal(value) {
        return (value && value.Object === Object) ? value : null;
    }

    var _ = {};
    _.debounce = debounce;
    _.throttle = throttle;
    // Expose lodash on the free variable `window` or `self` when available. This
    // prevents errors in cases where lodash is loaded by a script tag in the presence
    // of an AMD loader. See http://requirejs.org/docs/errors.html#mismatch for more details.
    (freeWindow || freeSelf || {})._ = _;

    // Some AMD build optimizers like r.js check for condition patterns like the following:
    if (typeof define == 'function' && typeof define.amd == 'object' && define.amd) {
        // Define as an anonymous module so, through path mapping, it can be
        // referenced as the "underscore" module.
        define(function () {
            return _;
        });
    }
        // Check for `exports` after `define` in case a build optimizer adds an `exports` object.
    else if (freeExports && freeModule) {
        // Export for Node.js.
        if (moduleExports) {
            (freeModule.exports = _)._ = _;
        }
        // Export for CommonJS support.
        freeExports._ = _;
    }
    else {
        // Export to the global object.
        root._ = _;
    }
}.call(this));

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const JIRA_CHECKLIST_CLASS = 'jira-checklist__checklist';
const JIRA_CHECK_ICON_CLASS = 'jira-checkbox__check-icon';
const JIRA_CHECKLIST_DESCRIPTION_CLASS = 'ak-renderer-document';
function stripHtml(html) {
    const tmpElem = document.createElement("DIV");
    tmpElem.innerHTML = html;
    return tmpElem.textContent || tmpElem.innerText || "";
}
const get = (obj, path, defaultValue = undefined) => {
    const travel = (regexp) => String.prototype.split
        .call(path, regexp)
        .filter(Boolean)
        .reduce((res, key) => (res !== null && res !== undefined ? res[key] : res), obj);
    const result = travel(/[,[\]]+?/) || travel(/[,[\].]+?/);
    return result === undefined || result === obj ? defaultValue : result;
};
function checkifyText(text) {
    text = text.replace(/(>|^)(\[x]|✅)/g, '$1<span class="jira-checkbox__check-icon">✅</span>');
    text = text.replace(/(>|^)(\[ ?]|⬜)/g, '$1<span class="jira-checkbox__check-icon">⬜</span>');
    return text;
}
function checkifyElement(element) {
    element.innerHTML = checkifyText(element.innerHTML);
}
/**
 * recursive function to checkify all json nodes of type text
 * @param doc
 */
function checkifyDoc(doc) {
    if (doc.content) {
        doc.content.forEach(checkifyDoc);
    }
    if (doc.type === 'text' && doc.text) {
        doc.text = stripHtml(checkifyText(doc.text));
    }
}
function toggleText(text) {
    text = text.replace('✅', '___unchecked-temp___');
    text = text.replace('⬜', '✅');
    text = text.replace('___unchecked-temp___', '⬜');
    return text;
}
function toggleCheckbox(element) {
    let issueDescription = element.innerHTML;
    issueDescription = toggleText(issueDescription);
    element.innerHTML = issueDescription;
}
function injectScript(file, node) {
    const body = document.getElementsByTagName(node)[0];
    const scriptElement = document.createElement('script');
    scriptElement.setAttribute('type', 'text/javascript');
    scriptElement.setAttribute('src', file);
    body.appendChild(scriptElement);
}
let issueId;
let descriptionDoc;
window.addEventListener("message", function (event) {
    console.log('received message in inject: ', event);
    if (event.data.source !== 'jira-checklist') {
        return;
    }
    const jiraInfo = event.data.payload;
    issueId = Object.keys(jiraInfo)[0];
    const issueFields = JSON.parse(jiraInfo[issueId].data.fields);
    const descriptionField = issueFields.find(field => field[0] === 'description');
    if (!descriptionField) {
        return null;
    }
    descriptionDoc = descriptionField[1].value;
}, false);
// Inject script into page so it can postMessage us the jira issue info
injectScript(chrome.extension.getURL('/src/inject/window-pass.js'), 'body');
function toggleCheckboxInDoc(clickedElemPath) {
    if (!descriptionDoc) {
        return;
    }
    // toggle checkbox for clicked elem
    const clickedJsonObject = get(descriptionDoc, clickedElemPath);
    checkifyDoc(descriptionDoc);
    if (clickedJsonObject.content && clickedJsonObject.content[0].text) {
        clickedJsonObject.content[0].text = toggleText(clickedJsonObject.content[0].text);
    }
}
function savePage() {
    return __awaiter(this, void 0, void 0, function* () {
        if (!descriptionDoc) {
            return;
        }
        const savePageUrl = `${location.origin}/rest/internal/3/issue/${issueId}/description`;
        console.log('saving page: ', descriptionDoc);
        const response = yield fetch(savePageUrl, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(descriptionDoc)
        });
        if (response.status < 200 || response.status >= 400) {
            console.error('save response has unexpected status code: ', { response, savePageUrl, descriptionDoc });
        }
    });
}
const savePageDebounced = _.debounce(savePage, 1000, { leading: false, trailing: true });
function buildJsonPath(clickedElement) {
    let elem = clickedElement.parentElement;
    let childIndexes = [];
    while (elem && !elem.classList.contains(JIRA_CHECKLIST_DESCRIPTION_CLASS)) {
        const childIndex = [...elem.parentElement.children].indexOf(elem);
        childIndexes.unshift(childIndex);
        elem = elem.parentElement;
    }
    return 'content[' + childIndexes.join('].content[') + ']';
}
chrome.extension.sendMessage({}, function (response) {
    const readyStateCheckInterval = setInterval(function () {
        if (document.readyState === "complete") {
            clearInterval(readyStateCheckInterval);
            // <ul class="ak-ul">
            // 		<li><p>[ ] todo 1</p></li>
            // 	  <li><p>[ ] todo 2</p></li>
            // 	  <li><p>[x] todo 3</p></li>
            // </ul>
            const issueDescriptionElem = document.querySelector('.' + JIRA_CHECKLIST_DESCRIPTION_CLASS);
            if (!issueDescriptionElem) {
                return;
            }
            const uls = issueDescriptionElem.querySelectorAll('ul');
            uls.forEach(ul => {
                const beforeModifyHtml = ul.innerHTML;
                ul.childNodes.forEach(checkifyElement);
                const afterModifyHtml = ul.innerHTML;
                if (beforeModifyHtml !== afterModifyHtml && !ul.classList.contains(JIRA_CHECKLIST_CLASS)) {
                    // This is a checklist ul
                    ul.classList.add(JIRA_CHECKLIST_CLASS);
                    ul.addEventListener('click', (evt) => {
                        if (evt.target) {
                            const clickedElem = evt.target;
                            if (clickedElem.classList.contains(JIRA_CHECK_ICON_CLASS)) {
                                // Toggle checkbox
                                toggleCheckbox(clickedElem);
                                evt.stopPropagation();
                                evt.preventDefault();
                                toggleCheckboxInDoc(buildJsonPath(clickedElem));
                                savePageDebounced();
                            }
                        }
                    });
                }
            });
        }
    }, 10);
});
