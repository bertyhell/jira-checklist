declare const chrome: any;
declare const _: any;

const JIRA_CHECKLIST_CLASS = 'jira-checklist__checklist';
const JIRA_CHECK_ICON_CLASS = 'jira-checkbox__check-icon';
const JIRA_CHECKLIST_DESCRIPTION_CLASS = 'ak-renderer-document';

interface JiraContent {
  version: number;
  type: string;
  text?: string;
  content?: JiraContent[];
  marks?: Mark[];
}

interface Mark {
  type: string;
  attrs: Attrs;
}

interface Attrs {
  href: string;
}

interface JiraFieldMeta {
  type: string;
  key: string;
  title?: string;
  editable?: boolean;
  value: any;
  renderer: any;
}


type JiraField = [string, JiraFieldMeta];

function stripHtml(html: string): string {
  const tmpElem = document.createElement("DIV");
  tmpElem.innerHTML = html;
  return tmpElem.textContent || tmpElem.innerText || "";
}

const get = (obj: any, path: string, defaultValue: any = undefined) => {
  const travel = (regexp: any) =>
      String.prototype.split
          .call(path, regexp)
          .filter(Boolean)
          .reduce((res, key) => (res !== null && res !== undefined ? res[key] : res), obj);
  const result = travel(/[,[\]]+?/) || travel(/[,[\].]+?/);
  return result === undefined || result === obj ? defaultValue : result;
};

function checkifyText(text: string): string {
  text = text.replace(/(>|^)(\[x]|✅)/g, '$1<span class="jira-checkbox__check-icon">✅</span>');
  text = text.replace(/(>|^)(\[ ?]|⬜)/g, '$1<span class="jira-checkbox__check-icon">⬜</span>');
  return text;
}

function checkifyElement(element: HTMLLIElement) {
  element.innerHTML = checkifyText(element.innerHTML);
}

/**
 * recursive function to checkify all json nodes of type text
 * @param doc
 */
function checkifyDoc(doc: JiraContent) {
  if (doc.content) {
    doc.content.forEach(checkifyDoc);
  }
  if (doc.type === 'text' && doc.text) {
    doc.text = stripHtml(checkifyText(doc.text));
  }
}

function toggleText(text: string): string {
  text = text.replace('✅', '___unchecked-temp___');
  text = text.replace('⬜', '✅');
  text = text.replace('___unchecked-temp___', '⬜');
  return text;
}

function toggleCheckbox(element: HTMLElement) {
  let issueDescription = element.innerHTML;
  issueDescription = toggleText(issueDescription);
  element.innerHTML = issueDescription;
}

function injectScript(file: string, node: string) {
  const body = document.getElementsByTagName(node)[0];
  const scriptElement = document.createElement('script');
  scriptElement.setAttribute('type', 'text/javascript');
  scriptElement.setAttribute('src', file);
  body.appendChild(scriptElement);
}

let issueId: string | undefined;
let descriptionDoc: JiraContent | undefined;
window.addEventListener("message", function(event) {
  console.log('received message in inject: ', event);
  if (event.data.source !== 'jira-checklist') {
    return;
  }

  const jiraInfo = event.data.payload;
  issueId = Object.keys(jiraInfo)[0];
  const issueFields: JiraField[] = JSON.parse(jiraInfo[issueId].data.fields);
  const descriptionField: JiraField | undefined = issueFields.find(field => field[0] === 'description');

  if (!descriptionField) {
    return null;
  }

  descriptionDoc = descriptionField[1].value;
}, false);


// Inject script into page so it can postMessage us the jira issue info
injectScript(chrome.extension.getURL('/src/inject/window-pass.js'), 'body');


function toggleCheckboxInDoc(clickedElemPath: string): void {
  if (!descriptionDoc) {
    return;
  }

  // toggle checkbox for clicked elem
  const clickedJsonObject: JiraContent = get(descriptionDoc, clickedElemPath);

  checkifyDoc(descriptionDoc);
  if (clickedJsonObject.content && clickedJsonObject.content[0].text) {
    clickedJsonObject.content[0].text = toggleText(clickedJsonObject.content[0].text);
  }
}

async function savePage() {
  if (!descriptionDoc) {
    return;
  }

  const savePageUrl = `${location.origin}/rest/internal/3/issue/${issueId}/description`;

  console.log('saving page: ', descriptionDoc);
  const response = await fetch(savePageUrl, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(descriptionDoc)
  });
  if (response.status < 200 || response.status >= 400) {
    console.error('save response has unexpected status code: ', { response, savePageUrl, descriptionDoc });
  }
}

const savePageDebounced = _.debounce(savePage, 1000, { leading: false, trailing: true });

function buildJsonPath(clickedElement: HTMLElement): string {
  let elem: HTMLElement = clickedElement.parentElement as HTMLElement;
  let childIndexes: number[] = [];
  while (elem && !elem.classList.contains(JIRA_CHECKLIST_DESCRIPTION_CLASS)) {
    const childIndex = [...(elem.parentElement as any).children].indexOf(elem);
    childIndexes.unshift(childIndex);

    elem = elem.parentElement as HTMLElement;
  }
  return 'content[' + childIndexes.join('].content[') + ']';
}

chrome.extension.sendMessage({}, function(response: any) {
  const readyStateCheckInterval = setInterval(function() {
    if (document.readyState === "complete") {
      clearInterval(readyStateCheckInterval);

      // <ul class="ak-ul">
      // 		<li><p>[ ] todo 1</p></li>
      // 	  <li><p>[ ] todo 2</p></li>
      // 	  <li><p>[x] todo 3</p></li>
      // </ul>

      const issueDescriptionElem = document.querySelector('.' + JIRA_CHECKLIST_DESCRIPTION_CLASS);
      if (!issueDescriptionElem) {
        return;
      }

      const uls = issueDescriptionElem.querySelectorAll('ul');
      uls.forEach(ul => {
        const beforeModifyHtml = ul.innerHTML;
        ul.childNodes.forEach(checkifyElement as any);
        const afterModifyHtml = ul.innerHTML;
        if (beforeModifyHtml !== afterModifyHtml && !ul.classList.contains(JIRA_CHECKLIST_CLASS)) {
          // This is a checklist ul
          ul.classList.add(JIRA_CHECKLIST_CLASS);

          ul.addEventListener('click', (evt: MouseEvent) => {
            if (evt.target) {
              const clickedElem = evt.target as HTMLElement;
              if (clickedElem.classList.contains(JIRA_CHECK_ICON_CLASS)) {
                // Toggle checkbox
                toggleCheckbox(clickedElem);
                evt.stopPropagation();
                evt.preventDefault();

                toggleCheckboxInDoc(buildJsonPath(clickedElem));
                savePageDebounced();
              }
            }
          });
        }
      });
    }
  }, 10);
});
